# Elasticsearch curator docker image
Docker image for the [Elasticsearch curator](https://www.elastic.co/guide/en/elasticsearch/client/curator)
based on the [CLARIN alpine base image](https://gitlab.com/CLARIN-ERIC/docker-alpine-base).

Intended for use within the [compose_fluent_kibana](https://gitlab.com/CLARIN-ERIC/compose_fluent_kibana)
project.
